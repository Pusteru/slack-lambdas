package com.serverless

import com.google.common.collect.Sets.combinations
import com.serverless.domain.People
import com.serverless.domain.WeightedPairs

class Scheduler {

    fun current(schedules: List<WeightedPairs>) = WeightedPairs(schedules.minBy { it.occurrences() }?.pairs
            ?: emptySet())

    fun convertFrom(pairs: WeightedPairs): List<WeightedPairs> {
        val people = pairs.toPeople()

        return combinations(pairs.pairs, people.numberOfPairs())
                .map { WeightedPairs(it) }
                .filter { possiblePairs -> possiblePairs.containsAll(people) }
                .let { addAlonePairs(it, people, pairs) }
    }

    private fun addAlonePairs(schedules: List<WeightedPairs>, people: People, pairs: WeightedPairs): List<WeightedPairs> {
        return schedules.map { schedule ->
            people.notContainIn(schedule)?.let {
                schedule.add(pairs.find(it))
            } ?: schedule
        }
    }
}