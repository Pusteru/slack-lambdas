package com.serverless.api

import com.jayway.jsonpath.JsonPath
import okhttp3.OkHttpClient
import okhttp3.Request
import java.time.LocalDate

class CalendarificClient {

    private val restClient = OkHttpClient.Builder().build()

    fun bankHolidays(year: Int): Set<LocalDate> = (holidaysFor("es-ct", year) + holidaysFor("es-b", year)).toSet()

    private fun holidaysFor(location: String, year: Int): List<LocalDate> {
        val apiUrl = "https://calendarific.com/api/v2/holidays?api_key=7712a0c8deebaedd0b33f2ac6d1598ffd934ae65&country=$location&year=$year"
        val request = Request.Builder().url(apiUrl)
                .get()
                .build()
        restClient.newCall(request).execute().use {
            return (JsonPath.read(it.body?.string(), "$.response.holidays[*].date.iso") as List<String>).map { date -> LocalDate.parse(date.split("T")[0]) }
        }
    }
}