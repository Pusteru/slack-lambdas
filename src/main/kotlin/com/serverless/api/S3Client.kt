package com.serverless.api

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.serverless.domain.People
import com.serverless.domain.WeightedPairs

private const val BUCKET = "schedule-s3"
private const val SCHEDULE_FILE = "Schedule.json"
private const val PAIRS_FILE = "Pairs.json"
private const val HOLIDAY_FILE = "Holiday.json"

class S3Client {
    private val s3client: AmazonS3 = AmazonS3ClientBuilder.standard().withRegion("eu-west-1").build()
    private val jacksonObjectMapper = jacksonObjectMapper()

    fun getPairs(): WeightedPairs {
        val schedule = s3client.getObject(BUCKET, PAIRS_FILE).objectContent
        return jacksonObjectMapper.readValue(schedule)
    }

    fun getSchedule(): WeightedPairs {
        val schedule = s3client.getObject(BUCKET, SCHEDULE_FILE).objectContent
        return jacksonObjectMapper.readValue(schedule)
    }

    fun storePairs(pairs: WeightedPairs) {
        s3client.putObject(BUCKET, PAIRS_FILE, jacksonObjectMapper.writeValueAsString(pairs))
    }


    fun storeSchedule(current: WeightedPairs) {
        s3client.putObject(BUCKET, SCHEDULE_FILE, jacksonObjectMapper.writeValueAsString(current))
    }

    fun getPeopleOnHoliday(): People {
        return if (s3client.doesObjectExist(BUCKET, HOLIDAY_FILE)) {
            jacksonObjectMapper.readValue(s3client.getObject(BUCKET, HOLIDAY_FILE).objectContent)
        } else {
            People(emptySet())
        }
    }

    fun storePeopleOnHoliday(people: People) {
        s3client.putObject(BUCKET, HOLIDAY_FILE, jacksonObjectMapper.writeValueAsString(people))
    }
}