package com.serverless.api

import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import java.util.logging.Logger

class SlackClient {

    private val webHook = "https://hooks.slack.com/services/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"
    private val webHookPrivate = "https://hooks.slack.com/services/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"
    private val logger = Logger.getLogger("")

    fun sendSlackNotification(currentSchedule: String) {
        val restClient = OkHttpClient.Builder().build()

        val request = Request.Builder()
                .url(webHook.toHttpUrlOrNull()!!)
                .post("{\"text\":\"$currentSchedule\"}".toRequestBody("application/json".toMediaTypeOrNull()))
                .build()

        restClient.newCall(request).execute().use {
            logger.info("response is: ${it.code}")
        }
    }
}