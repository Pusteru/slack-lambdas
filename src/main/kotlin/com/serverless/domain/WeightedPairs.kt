package com.serverless.domain

import com.fasterxml.jackson.annotation.JsonIgnore

data class WeightedPairs(val pairs: Set<WeightedPair>) {

    fun increaseOccurrencesWith(current: WeightedPairs): WeightedPairs {
        current.pairs.forEach { pair ->
            pairs.forEach {
                if (it == pair) {
                    it.occurrences++
                }
            }
        }
        return this
    }

    fun find(alonePerson: String) = pairs.find { it.first == alonePerson && it.second == alonePerson }!!

    fun removePairsWith(people: People) = copy(pairs = pairs.filter { it.first !in people.peopleList && it.second !in people.peopleList }.toSet())

    fun toPeople() = People(toPeoplesNames())

    fun containsAll(people: People) = toPeoplesNames().size / 2 == people.peopleList.size / 2

    fun add(alonePair: WeightedPair) = copy(pairs = this.pairs + alonePair)

    fun occurrences() = this.pairs.sumBy { it.occurrences }

    @JsonIgnore
    fun getMessage(): String {
        if (this.pairs.isEmpty()) return "Everybody is on holidays! :partyparrot:"
        return this.pairs.joinToString(separator = ",") { "${it.first} with ${it.second}" }
    }

    private fun toPeoplesNames() = this.pairs.map { weightedPair -> listOf(weightedPair.first, weightedPair.second) }.flatten().toSet()
}

data class WeightedPair(val first: String, val second: String, var occurrences: Int) {
    override fun equals(other: Any?): Boolean {
        if (other is WeightedPair) {
            if (this.first == other.second && this.second == other.first) {
                return true
            }
            if (this.first == other.first && this.second == other.second) {
                return true
            }
        }
        return false
    }

    override fun hashCode(): Int {
        return 1
    }
}
