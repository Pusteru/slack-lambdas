package com.serverless.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import com.google.common.collect.Sets

data class People(val peopleList: Set<String>) {

    fun add(people: People): People {
        return copy(peopleList = people.peopleList + peopleList)
    }

    fun numberOfPairs() = peopleList.size / 2

    @JsonIgnore
    fun isOdd() = peopleList.size % 2 != 0

    fun notContainIn(pairs: WeightedPairs): String? {
        val alonePeople = peopleList.toMutableSet()
        pairs.pairs.forEach {
            alonePeople.remove(it.first)
            alonePeople.remove(it.second)
        }
        return alonePeople.firstOrNull()
    }

    fun remove(people: People): People {
        return copy(peopleList = people.peopleList - peopleList)
    }
}