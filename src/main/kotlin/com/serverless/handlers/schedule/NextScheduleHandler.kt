package com.serverless.handlers.schedule

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.serverless.Scheduler
import com.serverless.api.S3Client
import com.serverless.config.ApiGatewayResponse
import com.serverless.domain.WeightedPairs

class NextScheduleHandler : RequestHandler<Map<String, Any>, ApiGatewayResponse> {

    private val s3client = S3Client()
    private val scheduler = Scheduler()

    override fun handleRequest(input: Map<String, Any>, context: Context): ApiGatewayResponse {
        val peopleOnHoliday = s3client.getPeopleOnHoliday()
        val weightedPairs = s3client.getPairs().removePairsWith(peopleOnHoliday)
        val schedule = scheduler.convertFrom(weightedPairs)
        val nextPairs = scheduler.current(schedule)
        s3client.storeSchedule(nextPairs)
        return response(nextPairs)
    }

    private fun response(nextSchedule: WeightedPairs): ApiGatewayResponse {
        val response = nextSchedule.getMessage()

        return ApiGatewayResponse.build {
            statusCode = 200
            objectBody = response
        }
    }
}
