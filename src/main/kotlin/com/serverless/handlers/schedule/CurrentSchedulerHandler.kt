package com.serverless.handlers.schedule

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.serverless.api.S3Client
import com.serverless.api.SlackClient
import com.serverless.config.ApiGatewayResponse


class CurrentSchedulerHandler : RequestHandler<Map<String, Any>, ApiGatewayResponse> {

    private val s3Client = S3Client()

    override fun handleRequest(input: Map<String, Any>, context: Context): ApiGatewayResponse {
        val current = s3Client.getSchedule()

        val currentScheduleMessage = current.getMessage()
        return response(currentScheduleMessage)
    }

    private fun response(nextSchedule: String): ApiGatewayResponse {
        return ApiGatewayResponse.build {
            statusCode = 200
            objectBody = nextSchedule
        }
    }
}
