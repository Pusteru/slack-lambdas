package com.serverless.handlers.schedule

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.serverless.api.S3Client
import com.serverless.config.ApiGatewayResponse

class IncreaseScheduleHandler : RequestHandler<Map<String, Any>, ApiGatewayResponse> {

    private val s3Client = S3Client()

    override fun handleRequest(input: Map<String, Any>, context: Context): ApiGatewayResponse {
        val current = s3Client.getSchedule()
        val pairs = s3Client.getPairs().increaseOccurrencesWith(current)
        s3Client.storePairs(pairs)
        return response()
    }

    private fun response(): ApiGatewayResponse {
        return ApiGatewayResponse.build {
            statusCode = 200
        }
    }
}
