package com.serverless.handlers.schedule

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.serverless.api.CalendarificClient
import com.serverless.api.S3Client
import com.serverless.api.SlackClient
import com.serverless.config.ApiGatewayResponse
import java.time.LocalDate


class SlackNotificatorHandler : RequestHandler<Map<String, Any>, ApiGatewayResponse> {

    private val s3Client = S3Client()
    private val slackClient = SlackClient()

    override fun handleRequest(input: Map<String, Any>, context: Context): ApiGatewayResponse {
        val holidays = CalendarificClient().bankHolidays(LocalDate.now().year)
        val currentScheduleMessage = if (holidays.contains(LocalDate.now())) {
            "No one is working today in Barcelona :face_palm:"
        } else {
            s3Client.getSchedule().getMessage()
        }
        slackClient.sendSlackNotification(currentScheduleMessage)
        return response(currentScheduleMessage)
    }

    private fun response(nextSchedule: String): ApiGatewayResponse {
        return ApiGatewayResponse.build {
            statusCode = 200
            objectBody = nextSchedule
        }
    }
}
