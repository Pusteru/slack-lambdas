package com.serverless.handlers.holiday

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.serverless.api.S3Client
import com.serverless.config.ApiGatewayResponse
import com.serverless.domain.People

class HolidayAddHandler : RequestHandler<Map<String, Any>, ApiGatewayResponse> {

    private val s3client = S3Client()
    private val jacksonObjectMapper = jacksonObjectMapper()

    override fun handleRequest(input: Map<String, Any>, context: Context): ApiGatewayResponse {
        val people: People = jacksonObjectMapper.readValue(input["body"].toString())
        val updatedPeople = s3client.getPeopleOnHoliday().add(people)
        s3client.storePeopleOnHoliday(updatedPeople)

        return response(updatedPeople)
    }

    private fun response(people: People): ApiGatewayResponse {
        return ApiGatewayResponse.build {
            statusCode = 200
            objectBody = jacksonObjectMapper.writeValueAsString(people)
        }
    }
}
