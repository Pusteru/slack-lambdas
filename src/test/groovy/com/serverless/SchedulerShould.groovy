package com.serverless

import com.serverless.domain.WeightedPair
import com.serverless.domain.WeightedPairs
import spock.lang.Specification

class SchedulerShould extends Specification {

    Scheduler scheduler = new Scheduler()

    def "calculate pair that least work together with no pairs"() {
        expect:
        scheduler.current([new WeightedPairs([].toSet())]) == new WeightedPairs([].toSet())
    }

    def "calculate pair that least work together"() {
        given:
        def permutations = [createSchedule([pair("BEN", "PAU", 1), pair("DAVID", "RADEK", 0), pair("MARC", "LLUIS", 0)]),
                            createSchedule([pair("BEN", "PAU", 1), pair("DAVID", "MARC", 0), pair("RADEK", "LLUIS", 0)]),
                            createSchedule([pair("BEN", "PAU", 1), pair("DAVID", "LLUIS", 0), pair("RADEK", "MARC", 0)]),
                            createSchedule([pair("BEN", "DAVID", 1), pair("PAU", "RADEK", 0), pair("MARC", "LLUIS", 0)]),
                            createSchedule([pair("BEN", "DAVID", 1), pair("PAU", "MARC", 0), pair("RADEK", "LLUIS", 0)]),
                            createSchedule([pair("BEN", "DAVID", 1), pair("PAU", "LLUIS", 0), pair("RADEK", "MARC", 0)]),
                            createSchedule([pair("BEN", "RADEK", 1), pair("PAU", "DAVID", 0), pair("MARC", "LLUIS", 0)]),
                            createSchedule([pair("BEN", "RADEK", 1), pair("PAU", "MARC", 0), pair("DAVID", "LLUIS", 0)]),
                            createSchedule([pair("BEN", "RADEK", 1), pair("PAU", "LLUIS", 0), pair("DAVID", "MARC", 0)]),
                            createSchedule([pair("BEN", "MARC", 1), pair("PAU", "DAVID", 0), pair("RADEK", "LLUIS", 0)]),
                            createSchedule([pair("BEN", "MARC", 1), pair("PAU", "RADEK", 0), pair("DAVID", "LLUIS", 0)]),
                            createSchedule([pair("BEN", "MARC", 0), pair("PAU", "LLUIS", 0), pair("DAVID", "RADEK", 0)]),
                            createSchedule([pair("BEN", "LLUIS", 1), pair("PAU", "DAVID", 0), pair("RADEK", "MARC", 0)]),
                            createSchedule([pair("BEN", "LLUIS", 1), pair("PAU", "RADEK", 0), pair("DAVID", "MARC", 0)]),
                            createSchedule([pair("BEN", "LLUIS", 1), pair("PAU", "MARC", 0), pair("DAVID", "RADEK", 0)])]

        when:
        def result = scheduler.current(permutations)

        then:
        result == new WeightedPairs([pair("BEN", "MARC", 0),
                                     pair("DAVID", "RADEK", 0),
                                     pair("PAU", "LLUIS", 0)].toSet())
    }

    def "calculate pair that least work together with odd numbers"() {
        given:
        def permutations = [createSchedule([pair("BEN", "PAU", 1), pair("DAVID", "RADEK", 0), pair("MARC", "MARC", 0)]),
                            createSchedule([pair("BEN", "PAU", 1), pair("DAVID", "MARC", 0), pair("RADEK", "RADEK", 0)]),
                            createSchedule([pair("BEN", "PAU", 1), pair("DAVID", "DAVID", 0), pair("RADEK", "MARC", 0)]),
                            createSchedule([pair("BEN", "DAVID", 1), pair("PAU", "RADEK", 0), pair("MARC", "MARC", 0)]),
                            createSchedule([pair("BEN", "DAVID", 1), pair("PAU", "MARC", 0), pair("RADEK", "RADEK", 0)]),
                            createSchedule([pair("BEN", "DAVID", 1), pair("PAU", "PAU", 0), pair("RADEK", "MARC", 0)]),
                            createSchedule([pair("BEN", "RADEK", 1), pair("PAU", "DAVID", 0), pair("MARC", "MARC", 0)]),
                            createSchedule([pair("BEN", "RADEK", 1), pair("PAU", "MARC", 0), pair("DAVID", "DAVID", 0)]),
                            createSchedule([pair("BEN", "RADEK", 1), pair("PAU", "PAU", 0), pair("DAVID", "MARC", 0)]),
                            createSchedule([pair("BEN", "MARC", 1), pair("PAU", "DAVID", 0), pair("RADEK", "RADEK", 0)]),
                            createSchedule([pair("BEN", "MARC", 1), pair("PAU", "RADEK", 0), pair("DAVID", "DAVID", 0)]),
                            createSchedule([pair("BEN", "MARC", 0), pair("PAU", "PAU", 0), pair("DAVID", "RADEK", 0)]),
                            createSchedule([pair("BEN", "BEN", 1), pair("PAU", "DAVID", 0), pair("RADEK", "MARC", 0)]),
                            createSchedule([pair("BEN", "BEN", 1), pair("PAU", "RADEK", 0), pair("DAVID", "MARC", 0)]),
                            createSchedule([pair("BEN", "BEN", 1), pair("PAU", "MARC", 0), pair("DAVID", "RADEK", 0)])]

        when:
        def result = scheduler.current(permutations)

        then:
        result == new WeightedPairs([pair("BEN", "MARC", 0),
                                     pair("DAVID", "RADEK", 0),
                                     pair("PAU", "PAU", 0)].toSet())
    }

    def "create a schedule from no persons"() {
        expect:
        def pairs = new WeightedPairs([].toSet())
        scheduler.convertFrom(pairs) == [new WeightedPairs([].toSet())]
    }

    def "create a schedule from a persons"() {
        expect:
        def pairs = new WeightedPairs([pair("BEN", "BEN", 0)].toSet())
        scheduler.convertFrom(pairs) == [createSchedule([pair("BEN", "BEN", 0)])]
    }

    def "create a schedule from two persons"() {
        expect:
        def pairs = new WeightedPairs([pair("DAVID", "BEN", 0)].toSet())
        scheduler.convertFrom(pairs) == [createSchedule([pair("DAVID", "BEN", 0)])]
    }

    def "create a schedule from three persons"() {
        expect:
        def pairs = new WeightedPairs([pair("DAVID", "BEN", 0),
                                       pair("DAVID", "PAU", 0),
                                       pair("PAU", "BEN", 0),
                                       pair("DAVID", "DAVID", 0),
                                       pair("BEN", "BEN", 0),
                                       pair("PAU", "PAU", 0)].toSet())
        scheduler.convertFrom(pairs) == [createSchedule([pair("DAVID", "BEN", 0), pair("PAU", "PAU", 0)]),
                                         createSchedule([pair("DAVID", "PAU", 0), pair("BEN", "BEN", 0)]),
                                         createSchedule([pair("PAU", "BEN", 0), pair("DAVID", "DAVID", 0)])]
    }

    def "create a schedule from a list of pairs"() {
        given:
        def pairs = new WeightedPairs([
                pair("BEN", "DWANE", 0),
                pair("DWANE", "DWANE", 0),
                pair("PAU", "PAU", 2),
                pair("DAVID", "DAVID", 2),
                pair("LLUIS", "DWANE", 2),
                pair("BEN", "BEN", 2),
                pair("RADEK", "DWANE", 2),
                pair("MARC", "MARC", 2),
                pair("MARC", "DWANE", 2),
                pair("DAVID", "DWANE", 2),
                pair("RADEK", "RADEK", 2),
                pair("LLUIS", "LLUIS", 3),
                pair("BEN", "LLUIS", 3),
                pair("PAU", "DWANE", 3),
                pair("PAU", "DAVID", 3),
                pair("MARC", "LLUIS", 3),
                pair("DAVID", "RADEK", 4),
                pair("PAU", "LLUIS", 4),
                pair("BEN", "MARC", 5),
                pair("PAU", "MARC", 5),
                pair("BEN", "RADEK", 5),
                pair("DAVID", "LLUIS", 5),
                pair("DAVID", "MARC", 5),
                pair("RADEK", "LLUIS", 5),
                pair("BEN", "PAU", 5),
                pair("RADEK", "MARC", 6),
                pair("BEN", "DAVID", 6),
                pair("PAU", "RADEK", 6)
        ].toSet())


        when:
        def result = scheduler.convertFrom(pairs)

        then:
        result == [createSchedule([pair("PAU", "DWANE", 3), pair("BEN", "DAVID", 6), pair("RADEK", "LLUIS", 5), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("BEN", "DAVID", 6), pair("MARC", "LLUIS", 3), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("BEN", "RADEK", 5), pair("MARC", "LLUIS", 3), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("BEN", "LLUIS", 3), pair("DAVID", "RADEK", 4), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("MARC", "LLUIS", 3), pair("DAVID", "RADEK", 4), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("BEN", "LLUIS", 3), pair("DAVID", "MARC", 5), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("BEN", "RADEK", 5), pair("DAVID", "MARC", 5), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("BEN", "RADEK", 5), pair("DAVID", "LLUIS", 5), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("RADEK", "LLUIS", 5), pair("DAVID", "MARC", 5), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("RADEK", "LLUIS", 5), pair("BEN", "MARC", 5), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("DAVID", "RADEK", 4), pair("BEN", "MARC", 5), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("BEN", "LLUIS", 3), pair("RADEK", "MARC", 6), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("BEN", "DAVID", 6), pair("RADEK", "MARC", 6), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("BEN", "DAVID", 6), pair("RADEK", "LLUIS", 5), pair("MARC", "DWANE", 2), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("BEN", "LLUIS", 3), pair("MARC", "DWANE", 2), pair("DAVID", "RADEK", 4), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("MARC", "LLUIS", 3), pair("DAVID", "RADEK", 4), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("BEN", "RADEK", 5), pair("MARC", "LLUIS", 3), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("RADEK", "LLUIS", 5), pair("DAVID", "MARC", 5), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("BEN", "RADEK", 5), pair("LLUIS", "DWANE", 2), pair("DAVID", "MARC", 5), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("BEN", "LLUIS", 3), pair("PAU", "RADEK", 6), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("BEN", "LLUIS", 3), pair("MARC", "DWANE", 2), pair("PAU", "RADEK", 6), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("BEN", "DAVID", 6), pair("MARC", "DWANE", 2), pair("PAU", "RADEK", 6), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("MARC", "LLUIS", 3), pair("PAU", "RADEK", 6), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("MARC", "LLUIS", 3), pair("PAU", "RADEK", 6), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("BEN", "DAVID", 6), pair("MARC", "LLUIS", 3), pair("PAU", "RADEK", 6), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("BEN", "DAVID", 6), pair("LLUIS", "DWANE", 2), pair("PAU", "RADEK", 6), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("BEN", "LLUIS", 3), pair("DAVID", "MARC", 5), pair("PAU", "RADEK", 6), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("DAVID", "MARC", 5), pair("PAU", "RADEK", 6), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("LLUIS", "DWANE", 2), pair("DAVID", "MARC", 5), pair("PAU", "RADEK", 6), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("RADEK", "LLUIS", 5), pair("PAU", "DAVID", 3), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("BEN", "LLUIS", 3), pair("MARC", "DWANE", 2), pair("PAU", "DAVID", 3), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("BEN", "RADEK", 5), pair("MARC", "DWANE", 2), pair("PAU", "DAVID", 3), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("RADEK", "LLUIS", 5), pair("MARC", "DWANE", 2), pair("PAU", "DAVID", 3), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("MARC", "LLUIS", 3), pair("PAU", "DAVID", 3), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("BEN", "RADEK", 5), pair("MARC", "LLUIS", 3), pair("PAU", "DAVID", 3), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("BEN", "RADEK", 5), pair("LLUIS", "DWANE", 2), pair("PAU", "DAVID", 3), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("BEN", "LLUIS", 3), pair("PAU", "MARC", 5), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("BEN", "RADEK", 5), pair("PAU", "MARC", 5), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("RADEK", "LLUIS", 5), pair("PAU", "MARC", 5), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("RADEK", "LLUIS", 5), pair("PAU", "MARC", 5), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("BEN", "DAVID", 6), pair("RADEK", "LLUIS", 5), pair("PAU", "MARC", 5), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("BEN", "DAVID", 6), pair("LLUIS", "DWANE", 2), pair("PAU", "MARC", 5), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("BEN", "RADEK", 5), pair("LLUIS", "DWANE", 2), pair("PAU", "MARC", 5), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("BEN", "LLUIS", 3), pair("DAVID", "RADEK", 4), pair("PAU", "MARC", 5), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("DAVID", "RADEK", 4), pair("PAU", "MARC", 5), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("LLUIS", "DWANE", 2), pair("DAVID", "RADEK", 4), pair("PAU", "MARC", 5), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("BEN", "RADEK", 5), pair("MARC", "DWANE", 2), pair("DAVID", "LLUIS", 5), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("PAU", "RADEK", 6), pair("DAVID", "LLUIS", 5), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("MARC", "DWANE", 2), pair("PAU", "RADEK", 6), pair("DAVID", "LLUIS", 5), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("PAU", "MARC", 5), pair("DAVID", "LLUIS", 5), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("BEN", "RADEK", 5), pair("PAU", "MARC", 5), pair("DAVID", "LLUIS", 5), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("BEN", "RADEK", 5), pair("PAU", "LLUIS", 4), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("BEN", "DAVID", 6), pair("MARC", "DWANE", 2), pair("PAU", "LLUIS", 4), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("BEN", "RADEK", 5), pair("MARC", "DWANE", 2), pair("PAU", "LLUIS", 4), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("LLUIS", "DWANE", 2), pair("DAVID", "RADEK", 4), pair("BEN", "MARC", 5), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("DAVID", "RADEK", 4), pair("PAU", "LLUIS", 4), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("MARC", "DWANE", 2), pair("DAVID", "RADEK", 4), pair("PAU", "LLUIS", 4), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("DAVID", "MARC", 5), pair("PAU", "LLUIS", 4), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("BEN", "RADEK", 5), pair("DAVID", "MARC", 5), pair("PAU", "LLUIS", 4), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("RADEK", "LLUIS", 5), pair("BEN", "MARC", 5), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("PAU", "RADEK", 6), pair("BEN", "MARC", 5), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("LLUIS", "DWANE", 2), pair("PAU", "RADEK", 6), pair("BEN", "MARC", 5), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("RADEK", "LLUIS", 5), pair("PAU", "DAVID", 3), pair("BEN", "MARC", 5), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("LLUIS", "DWANE", 2), pair("PAU", "DAVID", 3), pair("BEN", "MARC", 5), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("DAVID", "LLUIS", 5), pair("BEN", "MARC", 5), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("PAU", "RADEK", 6), pair("DAVID", "LLUIS", 5), pair("BEN", "MARC", 5), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("PAU", "LLUIS", 4), pair("BEN", "MARC", 5), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("DAVID", "RADEK", 4), pair("PAU", "LLUIS", 4), pair("BEN", "MARC", 5), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("BEN", "DAVID", 6), pair("MARC", "LLUIS", 3), pair("RADEK", "DWANE", 2), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("BEN", "LLUIS", 3), pair("DAVID", "MARC", 5), pair("RADEK", "DWANE", 2), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("BEN", "LLUIS", 3), pair("PAU", "DAVID", 3), pair("RADEK", "DWANE", 2), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("MARC", "LLUIS", 3), pair("PAU", "DAVID", 3), pair("RADEK", "DWANE", 2), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("BEN", "LLUIS", 3), pair("PAU", "MARC", 5), pair("RADEK", "DWANE", 2), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("BEN", "DAVID", 6), pair("PAU", "MARC", 5), pair("RADEK", "DWANE", 2), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("PAU", "MARC", 5), pair("DAVID", "LLUIS", 5), pair("RADEK", "DWANE", 2), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("BEN", "DAVID", 6), pair("PAU", "LLUIS", 4), pair("RADEK", "DWANE", 2), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("DAVID", "MARC", 5), pair("PAU", "LLUIS", 4), pair("RADEK", "DWANE", 2), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("PAU", "DAVID", 3), pair("BEN", "MARC", 5), pair("RADEK", "DWANE", 2), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("DAVID", "LLUIS", 5), pair("BEN", "MARC", 5), pair("RADEK", "DWANE", 2), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("PAU", "LLUIS", 4), pair("BEN", "MARC", 5), pair("RADEK", "DWANE", 2), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("BEN", "LLUIS", 3), pair("RADEK", "MARC", 6), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("BEN", "DAVID", 6), pair("LLUIS", "DWANE", 2), pair("RADEK", "MARC", 6), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("BEN", "LLUIS", 3), pair("PAU", "DAVID", 3), pair("RADEK", "MARC", 6), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("PAU", "DAVID", 3), pair("RADEK", "MARC", 6), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("LLUIS", "DWANE", 2), pair("PAU", "DAVID", 3), pair("RADEK", "MARC", 6), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("PAU", "DWANE", 3), pair("DAVID", "LLUIS", 5), pair("RADEK", "MARC", 6), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("DAVID", "LLUIS", 5), pair("RADEK", "MARC", 6), pair("PAU", "PAU", 2)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("PAU", "LLUIS", 4), pair("RADEK", "MARC", 6), pair("BEN", "BEN", 2)]),
                   createSchedule([pair("BEN", "DWANE", 0), pair("PAU", "LLUIS", 4), pair("RADEK", "MARC", 6), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("BEN", "DAVID", 6), pair("PAU", "LLUIS", 4), pair("RADEK", "MARC", 6), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("RADEK", "LLUIS", 5), pair("BEN", "PAU", 5), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("RADEK", "LLUIS", 5), pair("MARC", "DWANE", 2), pair("BEN", "PAU", 5), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("MARC", "LLUIS", 3), pair("BEN", "PAU", 5), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("MARC", "DWANE", 2), pair("DAVID", "RADEK", 4), pair("BEN", "PAU", 5), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("MARC", "LLUIS", 3), pair("DAVID", "RADEK", 4), pair("BEN", "PAU", 5), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("LLUIS", "DWANE", 2), pair("DAVID", "RADEK", 4), pair("BEN", "PAU", 5), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("RADEK", "LLUIS", 5), pair("DAVID", "MARC", 5), pair("BEN", "PAU", 5), pair("DWANE", "DWANE", 0)]),
                   createSchedule([pair("LLUIS", "DWANE", 2), pair("DAVID", "MARC", 5), pair("BEN", "PAU", 5), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("MARC", "DWANE", 2), pair("DAVID", "LLUIS", 5), pair("BEN", "PAU", 5), pair("RADEK", "RADEK", 2)]),
                   createSchedule([pair("MARC", "LLUIS", 3), pair("RADEK", "DWANE", 2), pair("BEN", "PAU", 5), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("DAVID", "MARC", 5), pair("RADEK", "DWANE", 2), pair("BEN", "PAU", 5), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("DAVID", "LLUIS", 5), pair("RADEK", "DWANE", 2), pair("BEN", "PAU", 5), pair("MARC", "MARC", 2)]),
                   createSchedule([pair("DAVID", "DWANE", 2), pair("RADEK", "MARC", 6), pair("BEN", "PAU", 5), pair("LLUIS", "LLUIS", 3)]),
                   createSchedule([pair("LLUIS", "DWANE", 2), pair("RADEK", "MARC", 6), pair("BEN", "PAU", 5), pair("DAVID", "DAVID", 2)]),
                   createSchedule([pair("DAVID", "LLUIS", 5), pair("RADEK", "MARC", 6), pair("BEN", "PAU", 5), pair("DWANE", "DWANE", 0)])]

    }

    WeightedPairs createSchedule(List<WeightedPair> pairs) {
        new WeightedPairs(pairs.toSet())
    }

    WeightedPair pair(String first, String second, int weight) {
        return new WeightedPair(first, second, weight)
    }
}
