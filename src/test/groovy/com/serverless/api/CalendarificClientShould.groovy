package com.serverless.api

import spock.lang.Specification

import java.time.LocalDate

class CalendarificClientShould extends Specification {

    def "should return holidays in Barcelona"(){
        expect:
        def holidays = new CalendarificClient().bankHolidays(2020)
        assert holidays.size() == 30
        assert holidays.contains(LocalDate.parse("2020-01-01"))
    }
}
